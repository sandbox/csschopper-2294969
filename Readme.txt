
-- SUMMARY --

Tumblr modules provide to admin pages for Configure and set the Tumblr details.

How to use:-

1) Download Tumblr module from this source "http://drupal.org/project/tumblr"

2) copy the zip file in sites/all/module directory and extract them.

3) open the module list after login admin user and goes to Tumblr Package and checked both modules and install them.

4) In the admin section open the "admin/config/tumblr/settings" and fill the details of Tumblr.
    
    if you have not details then open this link "http://www.tumblr.com/oauth/register" and create new app for own site and put details here.
    
5) choose content type and its images fields ("open admin/config/tumblr/contenttype/settings") you want to post on tumblr site.


Advantage:-

Tumblr module post the node Title,body and images fields value on tumblr site on creation time. This module provide a admin section to put details and 

select content type and its fields to send data on Tumblr site which you have to configure them.

More Details:-

please visit this link for more details "http://drupal.org/project/tumblr"


Maintainer:=

http://www.sparxitsolutions.com/

 
